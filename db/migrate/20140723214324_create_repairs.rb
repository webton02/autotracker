class CreateRepairs < ActiveRecord::Migration
  def change
    create_table :repairs do |t|
      t.integer :vehicle_id
      t.string :part
      t.integer :cost
      t.date :date_completed

      t.timestamps
    end
  end
end
