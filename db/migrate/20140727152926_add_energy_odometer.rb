class AddEnergyOdometer < ActiveRecord::Migration
  def change
    change_table :vehicles do |t|
      t.string :energy_type
      t.integer :odometer_reading
    end
  end
end
