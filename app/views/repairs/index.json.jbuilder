json.array!(@repairs) do |repair|
  json.extract! repair, :id, :part, :cost, :date_completed
  json.url repair_url(repair, format: :json)
end
