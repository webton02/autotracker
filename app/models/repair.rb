class Repair < ActiveRecord::Base
  belongs_to :vehicle
  validates_presence_of :part
  validates_presence_of :cost
  validates_presence_of :date_completed
end
