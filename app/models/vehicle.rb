class Vehicle < ActiveRecord::Base
  has_many :repairs, dependent: :destroy
  validates_presence_of :brand
  validates_presence_of :model
end
